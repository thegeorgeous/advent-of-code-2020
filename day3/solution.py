#!/usr/bin/env python3

# Part - I

def day3(slope_right = 3, slope_down = 1):
    input_file = open("input.txt", "r")
    count = 0

    pos = 0
    for idx, line in enumerate(input_file):
        if idx == 0: continue

        if idx % slope_down != 0: continue

        if pos + slope_right <= 30:
            pos = pos + slope_right
        else:
            pos = 0 + slope_right - (31 - pos) # last position is 31

        if line[pos] == "#": count += 1

    return count

# print(day3(slope_right = 3, slope_down = 1))

# Part - 2

slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]

product = 1
for slope in slopes:
    product *= day3(slope_right = slope[0], slope_down = slope[1])

print(product)
