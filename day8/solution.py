#!/usr/bin/env python3

# Part - 1
# def day8():
#     with open('input.txt') as file:
#         instructions = file.readlines()

#     accumulator = 0
#     line = 0
#     visited_lines = []

#     while True:
#         if line in visited_lines: break
        
#         instruction, value = instructions[line].split(" ")
#         if instruction == "acc":
#             accumulator += int(value)
#             visited_lines.append(line)
#             line += 1
#         elif instruction == "jmp":
#             visited_lines.append(line)
#             line += int(value)
#         elif instruction == "nop":
#             line += 1

#     return accumulator
            
    
# print(day8())

def day8():
    with open('input.txt') as file:
        instructions = file.readlines()

    permutations = []

    for i in range(len(instructions)):
        if "jmp" in instructions[i]:
            instructions[i] = instructions[i].replace("jmp", "nop")
            permutations.append(instructions.copy())
            instructions[i] = instructions[i].replace("nop", "jmp")
        elif "nop" in instructions[i]:
            instructions[i] = instructions[i].replace("nop", "jmp")
            permutations.append(instructions.copy())
            instructions[i] = instructions[i].replace("jmp", "nop")

    for permutation in permutations:
        valid, acc = execute(permutation)
        if valid:
            return acc
            


def execute(instructions):
    accumulator = 0
    line = 0
    visited_lines = []

    while True:
        if line >= len(instructions):
            return True, accumulator
        if line in visited_lines:
            return False, accumulator
           
        instruction, value = instructions[line].split(" ")
        if instruction == "acc":
            accumulator += int(value)
            visited_lines.append(line)
            line += 1
        elif instruction == "jmp":
            visited_lines.append(line)
            line += int(value)
        elif instruction == "nop":
            line += 1

print(day8())
