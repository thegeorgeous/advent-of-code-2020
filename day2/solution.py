#!/usr/bin/env python3

input = open('input.txt', 'r')

# input = ["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"]

# Part - 1
# count = 0
# for line in input:
#     input_words = line.split(" ")
#     limits = input_words[0].split("-")
#     input_min = int(limits[0])
#     input_max = int(limits[1])

#     print(input_min, input_max)
#     char = input_words[1][0]
#     print(char)

#     password = input_words[2]
    
#     if input_min <= password.count(char) <= input_max:
#         count += 1

# print(count)

# Part - 2

count = 0
for line in input:
    input_words = line.split(" ")
    limits = input_words[0].split("-")
    pos1 = int(limits[0])-1 
    pos2 = int(limits[1])-1

    char = input_words[1][0]

    password = input_words[2]

    if (password[pos1] == char and password[pos2] != char) or (password[pos1] != char and password[pos2] == char):
        count += 1

print(count)
