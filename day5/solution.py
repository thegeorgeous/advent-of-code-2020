#!/usr/bin/env python3

# Part - 1
def day5():
    input_file = open("input.txt", "r")

    seat_ids = []
    
    for line in input_file:
        f = 0
        b = 127

        for i in range(0, 6):
            if line[i] == "F":
                b = (f+b)//2
            elif line[i] == "B":
                f = (b+f)//2 + 1

        row = min(f, b) if line[6] == "F" else max(f, b)

        l = 0
        r = 7
        for i in range(7, 9):
            if line[i] == "L":
                r = (l+r)//2
            elif line[i] == "R":
                l = (l+r)//2 + 1

        column = min(l, r) if line[9] == "L" else max(l, r)
        seat_ids.append((row * 8) + column)
    return seat_ids

# print(max(day5()))

# Part - 2

seat_ids = day5()
seat_ids.sort()

for i in range(1, len(seat_ids)):
    if seat_ids[i] - seat_ids[i-1] > 1:
        print(seat_ids[i] - 1)
