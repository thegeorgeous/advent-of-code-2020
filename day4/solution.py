#!/usr/bin/env python3

import copy
import re

# Part - 1
# def day4():
#     input_file = open("input.txt", "r")

#     count = 0

#     required_fields = {
#         "byr": None,
#         "iyr": None,
#         "eyr": None,
#         "hgt": None,
#         "hcl": None,
#         "ecl": None,
#         "pid": None
#     }

#     passport_fields = copy.deepcopy(required_fields)

#     for line in input_file:
#         if len(line) <= 1:
#             count += 1
#             for field, value in passport_fields.items():
#                 if not value:
#                     count -= 1
#                     break
            
#             passport_fields = copy.deepcopy(required_fields)
#             continue

#         fields = line.split(" ")

#         for field in fields:
#             field_name, value = field.split(":")
#             if field_name == "cid": continue
#             passport_fields[field_name] = value

#     return count

# print(day4())

# Part - 2
 
def day4():
    input_file = open("input.txt", "r")

    count = 0

    required_fields = {
        "byr": None,
        "iyr": None,
        "eyr": None,
        "hgt": None,
        "hcl": None,
        "ecl": None,
        "pid": None
    }

    passport_fields = copy.deepcopy(required_fields)

    for line in input_file:
        if len(line) <= 1:
            count += 1
            for field, value in passport_fields.items():
                if not value:
                    count -= 1
                    break
                
                if not validate(field, value):
                    count -= 1
                    break

            passport_fields = copy.deepcopy(required_fields)
            continue

        fields = line.split(" ")

        for field in fields:
            field_name, value = field.split(":")
            if field_name == "cid": continue
            passport_fields[field_name] = value.strip()

    return count

def validate(field, value):
    if field == 'byr':
        return len(value) == 4 and 1920 <= int(value) <= 2002
    elif field == 'iyr':
        return len(value) == 4 and 2010 <= int(value) <= 2020
    elif field == 'eyr':
        return len(value) == 4 and 2020 <= int(value) <= 2030
    elif field == 'hgt':
        hgt = value[:-2]
        dimension = value[-2:]
        if re.match(r"[0-9]", hgt) and dimension == "cm" and len(hgt) > 1 and (150 <= int(hgt) <= 193):
            return True
        elif re.match(r"[0-9]", hgt) and dimension == 'in' and len(hgt) > 1 and (59 <= int(hgt) <= 76):
            return True
        else: return False
    elif field == 'hcl':
        if len(value) == 7 and re.match(r"#[0-9a-z]", value):
            return True
        else: return False
    elif field == 'ecl':
        if value in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
            return True
    elif field == 'pid':
        return len(value) == 9
    else:
        return False

print(day4())
