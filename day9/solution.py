#!/usr/bin/env python3

def part1(file_name, preamble):
    with open(file_name, "r") as file:
        numbers = file.readlines()
    numbers = list(map(lambda x: int(x), numbers))

    for i in range(preamble+1, len(numbers)):
        if not valid(numbers[i - preamble:i], numbers[i]):
            return numbers[i]
        
        
def valid(nums, target):
    diff_map = {}
    for i in range(len(nums)):
        complement = target - nums[i]
        if complement in diff_map:
            return True
            
        diff_map[nums[i]] = i
    return False

    for i in batch:
        n = x - i
        if n in batch:
            return True
    return False
    
# print(part1("test.txt", 5))
# print(part1("input.txt", 25))

def part2(file_name, invalid_number):
    with open(file_name, "r") as file:
        numbers = file.readlines()
    numbers = list(map(lambda x: int(x), numbers))

    for i in range(len(numbers)):
        x = invalid_number
        j = i
        while True:
            x -= numbers[j]
            if x < 0: break
            if x == 0:
                batch = numbers[i:j+1]
                return min(batch) + max(batch)
            j += 1

# print(part2("test.txt", 127))
print(part2("input.txt", 15690279))

