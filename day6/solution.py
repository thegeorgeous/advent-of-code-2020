#!/usr/bin/env python3

# def day6():
#     input_file = open("input.txt", "r")

#     count = 0
#     questions = set()
#     for line in input_file:
#         if len(line.strip()) == 0:
#             count += len(questions)
#             questions = set()

#         for ch in list(line.strip()):
#             questions.add(ch)

#     return count

# print(day6())

def day6():
    input_file = open("input.txt", "r")

    count = 0
    questions = {}
    group_count = 0

    for line in input_file:
        if len(line.strip()) == 0:
            for question, answer_count in questions.items():
                if answer_count == group_count: 
                    count += 1
            questions = {}
            group_count = 0
            continue

        group_count += 1

        for ch in list(line.strip()):
            if ch in questions:
                questions[ch] += 1
            else:
                questions[ch] = 1

    return count

print(day6())
