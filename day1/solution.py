#!/usr/bin/env python3

input_file = open('input.txt', 'r')
# input_file = [1721,
#               979,
#               366,
#               299,
#               675,
#               1456]

# Part - 1

def two_sum(target = 2020):
    input_file = open('input.txt', 'r')
    complement_map = {}

    for line in input_file:
        number = int(line)
        if number < target:
            complement = target - number

            if number in complement_map:
                return number * complement_map[number]

            complement_map[complement] = number

# Part - 2

def three_sum(target = 2020):
    input_file = open('input.txt', 'r')
    for line in input_file:
        number = int(line)
        complement = target - number
        sum_2 = two_sum(complement)
        if sum_2:
            return sum_2 * number

print(three_sum())
